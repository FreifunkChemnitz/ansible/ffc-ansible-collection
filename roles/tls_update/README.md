# ansible-role-tls-update

This ansible role is for ACME compatible certificate request via DNS validation. Its using nsupdate with key challenge to change the DNS zone.

## requirements

### variables

* ```account_uri``` URL to the acme account
* ```acme_directory``` URL to the acme directory
* ```contact_address``` contact information for acme account
* ```dns_server``` IP of dns server to send the nsupdate to
* ```nsupdate_key``` secret of bind key for nsupdate
* ```nsupdate_name``` name of bind key for nsupdate

### files

* acme account privkey with filename ```domain.tld.acme.key```

## notes

### format of answer for challenge if no cert exists

```
TASK [tls : debug] ***************************************************************************************************
ok: [localhost] => {
    "msg": {
        "account_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/acct/[CENSORED]",
        "authorizations": {
            "*.[CENSORED]": {
                "challenges": [
                    {
                        "status": "pending",
                        "token": "L0aNOSXSp8sNow7htZOVh8o6GpGa_dIdC1C_miZEHQw",
                        "type": "dns-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/213711034/7-mFVg"
                    }
                ],
                "expires": "2021-03-01T22:24:44Z",
                "identifier": {
                    "type": "dns",
                    "value": "[CENSORED]"
                },
                "status": "pending",
                "uri": "https://acme-staging-v02.api.letsencrypt.org/acme/authz-v3/[CENSORED]",
                "wildcard": true
            },
            "[CENSORED]": {
                "challenges": [
                    {
                        "status": "pending",
                        "token": "Yg_Sg94pnMwb39g_R-tkjpGeIc_hwFuSdnR0l5Ed8E4",
                        "type": "http-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/213711036/wTzb9w"
                    },
                    {
                        "status": "pending",
                        "token": "Yg_Sg94pnMwb39g_R-tkjpGeIc_hwFuSdnR0l5Ed8E4",
                        "type": "dns-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/213711036/1YeVhw"
                    },
                    {
                        "status": "pending",
                        "token": "Yg_Sg94pnMwb39g_R-tkjpGeIc_hwFuSdnR0l5Ed8E4",
                        "type": "tls-alpn-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/213711036/n9EdGw"
                    }
                ],
                "expires": "2021-03-01T22:24:44Z",
                "identifier": {
                    "type": "dns",
                    "value": "[CENSORED]"
                },
                "status": "pending",
                "uri": "https://acme-staging-v02.api.letsencrypt.org/acme/authz-v3/[CENSORED]"
            }
        },
        "cert_days": -1,
        "challenge_data": {
            "*.[CENSORED]": {
                "dns-01": {
                    "record": "_acme-challenge.[CENSORED]",
                    "resource": "_acme-challenge",
                    "resource_value": "fFWruf2yTKEgEE5Y8ItPSU9v-F5BXAUo1Nzd0PWlW3M"
                }
            },
            "[CENSORED]": {
                "dns-01": {
                    "record": "_acme-challenge.[CENSORED]",
                    "resource": "_acme-challenge",
                    "resource_value": "Qw8nx0G4ivA2unHzE63e7dnmyMmkj1M1z1wvkcQXQio"
                },
                "http-01": {
                    "resource": ".well-known/acme-challenge/Yg_Sg94pnMwb39g_R-tkjpGeIc_hwFuSdnR0l5Ed8E4",
                    "resource_value": "Yg_Sg94pnMwb39g_R-tkjpGeIc_hwFuSdnR0l5Ed8E4.t__LpO0irV2LcnW7pjilS5T9FRI21iCh_rl_XibXMBg"
                },
                "tls-alpn-01": {
                    "resource": "[CENSORED]",
                    "resource_original": "dns:[CENSORED]",
                    "resource_value": "Qw8nx0G4ivA2unHzE63e7dnmyMmkj1M1z1wvkcQXQio="
                }
            }
        },
        "challenge_data_dns": {
            "_acme-challenge.[CENSORED]": [
                "fFWruf2yTKEgEE5Y8ItPSU9v-F5BXAUo1Nzd0PWlW3M",
                "Qw8nx0G4ivA2unHzE63e7dnmyMmkj1M1z1wvkcQXQio"
            ]
        },
        "changed": true,
        "failed": false,
        "finalize_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/finalize/18193312/245500171",
        "order_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/order/18193312/245500171"
    }
}
```


### format for challenge answer if there exists a certificate

```
TASK [tls : debug] ***************************************************************************************************
ok: [localhost] => {
    "msg": {
        "account_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/acct/[CENSORED]",
        "authorizations": {
            "*.[CENSORED]": {
                "challenges": [
                    {
                        "status": "valid",
                        "token": "wArJnXcpvkrLco3A5GsVG_cFg37UyAcxnsxxJ-te3_A",
                        "type": "dns-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/212467797/H-Y4lQ",
                        "validationRecord": [
                            {
                                "hostname": "[CENSORED]"
                            }
                        ]
                    }
                ],
                "expires": "2021-03-22T16:11:59Z",
                "identifier": {
                    "type": "dns",
                    "value": "[CENSORED]"
                },
                "status": "valid",
                "uri": "https://acme-staging-v02.api.letsencrypt.org/acme/authz-v3/[CENSORED]",
                "wildcard": true
            },
            "[CENSORED]": {
                "challenges": [
                    {
                        "status": "valid",
                        "token": "Vjlip_HXlhYjEtAcR4EVWnCo8TWeuLVLjCLIhyFz2Vs",
                        "type": "dns-01",
                        "url": "https://acme-staging-v02.api.letsencrypt.org/acme/chall-v3/212467798/WquJjQ",
                        "validationRecord": [
                            {
                                "hostname": "[CENSORED]"
                            }
                        ]
                    }
                ],
                "expires": "2021-03-22T16:12:13Z",
                "identifier": {
                    "type": "dns",
                    "value": "[CENSORED]"
                },
                "status": "valid",
                "uri": "https://acme-staging-v02.api.letsencrypt.org/acme/authz-v3/[CENSORED]"
            }
        },
        "cert_days": 89,
        "challenge_data": {},
        "challenge_data_dns": {},
        "changed": true,
        "failed": false,
        "finalize_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/finalize/18193312/245499698",
        "order_uri": "https://acme-staging-v02.api.letsencrypt.org/acme/order/18193312/245499698"
    }
}
```

## license

This work is licensed under [the Unlicense](https://unlicense.org/).
